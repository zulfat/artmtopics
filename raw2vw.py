import re
import os
import sys
import json
import codecs
import pandas as pd
from urlparse import urlparse
from joblib import Parallel, delayed
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag
from nltk.corpus import wordnet


def get_wordnet_pos(treebank_tag):

    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return wordnet.NOUN


def preprocess_text(text, lemmatizer, lemmas={}):
  if not isinstance(text, unicode): text = text.decode('utf-8')
  # tokenized = word_tokenize(text)
  tokenized = re.findall('\w+', text.lower())
  tokenized = pos_tag(tokenized)
  processed_text = []
  pos_tags = []
  for token, ptag in tokenized:
    ltoken = token
    wnet_pos = get_wordnet_pos(ptag)
    if token + wnet_pos not in lemmas:
      lemmas[token + wnet_pos] = lemmatizer.lemmatize(token, wnet_pos)
    ltoken = lemmas[token + wnet_pos]
    processed_text.append(ltoken)
    pos_tags.append(wnet_pos)

  processed_data = {'text':u' '.join(processed_text).encode('utf-8'),
                    'pos_tags': u' '.join(pos_tags).encode('utf-8')}
  return processed_data


def process_file(inputfpath, outputfpath):
  lemmatizer = WordNetLemmatizer()
  lemmas = {}
  df = pd.read_csv(inputfpath, names=['url', 'text'], sep=',', quotechar='"', quoting=0)
  df = df[~df.text.isnull()]
  with codecs.open(outputfpath, 'w') as output_stream:
    for row_index, row in df.iterrows():
      line, url = row['text'], row['url']
      if len(line) <= 3: continue
      processed = preprocess_text(line, lemmatizer, lemmas)
      processed['url'] = url or 'nourl'
      dumped_str = json.dumps(processed)
      output_stream.write('{}\n'.format(dumped_str))
  print "Processed", inputfpath

if __name__ == '__main__':
  input_folder = '/data/solidopinion/raw_data'
  output_folder = '/data/solidopinion/vw'
  files = []
  for fname in os.listdir(input_folder):
    inputfpath = os.path.join(input_folder, fname)
    outputfpath = os.path.join(output_folder, fname)
    files.append((inputfpath, outputfpath))
  Parallel(n_jobs=4)(delayed(process_file)(*iofiles) for iofiles in files)
