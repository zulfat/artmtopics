import sys
import codecs
from glob import glob


def update_vocab(vocab, fpath):
  dcount = 0
  with codecs.open(fpath) as input_stream:
    for line in input_stream:
      dcount += 1
      splitted_line = line.split()
      token_counts = splitted_line[1:]
      for token_count in token_counts:
        token, count = token_count.split(':')
        if token not in vocab: vocab[token] = 0
        vocab[token] += 1
  return dcount


if __name__ == '__main__':
  vocab = {}
  inputfolder = '/data/solidopinion/vw/*'
  vocab_output_path = '/data/solidopinion/vocab.txt'
  docs_count = 0
  for fpath in glob(inputfolder):
    docs_count += update_vocab(vocab, fpath)

  with codecs.open(vocab_output_path, 'w') as output_stream:
    for token, count in vocab.items():
      if count <= 4 or count > 0.4*docs_count: continue
      output_stream.write('{}\n'.format(token))
