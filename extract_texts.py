from argparse import ArgumentParser
from glob import glob
import pandas as pd
import os


if __name__ == '__main__':
  argparser = ArgumentParser()
  argparser.add_argument('--input_directory')
  argparser.add_argument('--output_directory')
  args = argparser.parse_args()
  infiles_pattern = os.path.join(args.input_directory, '*.csv')
  infile_columns = ['url', 'text']
  for infile_path in glob(infiles_pattern):
    df = pd.read_csv(infile_path, names=infile_columns, encoding='utf-8')
    fname = os.path.basename(infile_path)
    output_path = os.path.join(args.output_directory, fname)
    df.text.to_csv(output_path, encoding='utf-8', index=False, header=None)
