import os
import json
import codecs
from glob import glob
from nltk.corpus import stopwords
from joblib import Parallel, delayed

STOPWORDS = stopwords.words('english')


def iterate_ngrams(document, n):
  document_length = len(document)
  for start_pos in range(document_length):
    for end_pos in range(start_pos+1, start_pos+1+n):
      if any([token in STOPWORDS for token in document[start_pos:end_pos]]): continue
      yield '_'.join(document[start_pos:end_pos])


def get_vw_formatted(document, docid):
  word_counts = {}
  for ngram in iterate_ngrams(document['text'].split(), 2):
    # already not in stopwords
    # if ngram in STOPWORDS: continue
    if ngram not in word_counts: word_counts[ngram] = 0
    word_counts[ngram] += 1
  vw_formatted = [docid]
  for ngram, count in word_counts.items():
    vw_formatted.append('{}:{}'.format(ngram, count))
  return ' '.join(vw_formatted)



def json2vw(inputfpath, outputfpath, ngrams=2):
  with codecs.open(inputfpath) as input_stream:
    collection = map(json.loads, input_stream)
  with codecs.open(outputfpath, 'w') as output_stream:
    for i, document in enumerate(collection):
      vw_formatted = get_vw_formatted(document, inputfpath + '_' + str(i))
      if ' ' not in vw_formatted: continue
      output_stream.write('{}\n'.format(vw_formatted))
  print "Processed", inputfpath

if __name__ == '__main__':
  inputfpath_pattern = '/data/solidopinion/json/*.csv'
  outputfolder = '/data/solidopinion/vw'
  inputfpaths = glob(inputfpath_pattern)
  files = []
  for inputfpath in inputfpaths:
    basename = os.path.basename(inputfpath)
    outputfpath = os.path.join(outputfolder, basename)
    files.append((inputfpath, outputfpath))
  Parallel(n_jobs=4)(delayed(json2vw)(*iofiles) for iofiles in files)
