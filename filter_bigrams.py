import codecs
from glob import glob
from joblib import Parallel, delayed


def update_stats(bigram_stats, unigram_stats, inputfpath):
  with codecs.open(inputfpath) as input_stream:
    for line in input_stream:
      for ngram_count in line.split()[1:]:
        ngram, count = ngram_count.split(':')
        if '_' not in ngram: 
          if ngram not in unigram_stats: unigram_stats[ngram] = 0
          unigram_stats[ngram] += 1
        else:
          if ngram not in bigram_stats: bigram_stats[ngram] = 0
          bigram_stats[ngram] += 1


def filter_data(inputfpath, bigrams):
  with codecs.open(inputfpath) as input_stream:
    collection = input_stream.readlines()
  with codecs.open(inputfpath, 'w') as output_stream:
    for document in collection:
      doc_parts = document.split()
      new_doc = [doc_parts[0]]
      for ngram_count in doc_parts[1:]:
        ngram, count = ngram_count.strip().split(':')
        if '_' not in ngram or ngram in bigrams: new_doc.append(ngram_count)
      new_doc = ' '.join(new_doc)
      output_stream.write('{}\n'.format(new_doc))


def calc_pmi(bigram_stats, unigram_stats):
  bigram_pmis = {}
  for bigram, joint_count in bigram_stats.items():
    w1, w2 = bigram.split('_')
    w1_count = unigram_stats[w1]
    w2_count = unigram_stats[w2]
    bigram_pmis[bigram] = 1.0*joint_count/w1_count/w2_count
  return bigram_pmis


if __name__ == '__main__':
  inputfpattern = '/data/solidopinion/vw/*.csv'
  bigram_stats = {}
  unigram_stats = {}
  topn = 10000
  for inputfpath in glob(inputfpattern):
    update_bigrams(bigrams, inputfpath)
  bigram_pmis = calc_pmi(bigram_stats, unigram_stats)
  bigrams = sorted(bigram_pmis, key=lambda ngram: bigram_pmis[ngram], reverse=True)[:topn]
  Parallel(n_jobs=4)(delayed(filter_data)(inputfpath, bigrams) for inputfpath in glob(inputfpattern)