import artm
from argparse import ArgumentParser


if __name__ == '__main__':
  argparser = ArgumentParser()
  argparser.add_argument('--data_path')
  argparser.add_argument('--format', default='vowpal_wabbit')
  argparser.add_argument('--name', default='solidopinion')
  argparser.add_argument('--target_folder')
  args = argparser.parse_args()

  # preparing data loader
  batch_vectorizer = artm.BatchVectorizer(data_path=args.data_path, data_format=args.format,
                                          collection_name=args.name, target_folder=args.target_folder)
  dictionary = batch_vectorizer.dictionary
  print dictionary
  # defining the model
  subject_topic_names = ['subject_topic_{}'.format(i) for i in xrange(20)]
  background_topic_names = ['background_topic_{}'.format(i) for i in xrange(5)]
  # defining regularizers
  sparse_theta_subject = artm.SmoothSparseThetaRegularizer(name='SparseTheta_sub', topic_names=subject_topic_names, tau=-0.15)
  sparse_theta_background = artm.SmoothSparseThetaRegularizer(name='SparseTheta_back', topic_names=background_topic_names, tau=0.15)
  sparse_phi_subject = artm.SmoothSparsePhiRegularizer(name='SparsePhi_sub', topic_names=subject_topic_names, tau=-0.1)
  sparse_phi_background = artm.SmoothSparsePhiRegularizer(name='SparsePhi_back', topic_names=background_topic_names, tau=0.1)
  decorellate_phi_subject = artm.DecorrelatorPhiRegularizer(name='DecorrelatorPhi_sub', topic_names=subject_topic_names, tau=1.5e+5)
  decorellate_phi_background = artm.DecorrelatorPhiRegularizer(name='DecorrelatorPhi_back', topic_names=background_topic_names, tau=1.5e+5)
  model_artm = artm.ARTM(topic_names=subject_topic_names+background_topic_names, cache_theta=True, dictionary=dictionary,
                         regularizers=[sparse_theta_subject, sparse_theta_background, sparse_phi_subject, sparse_phi_background, decorellate_phi_subject, decorellate_phi_background], show_progress_bars=True)
  model_artm.scores.add(artm.TopTokensScore(name='TopTokensScore', num_tokens=10))
  model_artm.num_document_passes = 1
  print "Fitting the model"
  model_artm.fit_offline(batch_vectorizer=batch_vectorizer, num_collection_passes=35)
  for topic_name in model_artm.topic_names:
    print topic_name + ': ',
    print model_artm.score_tracker['TopTokensScore'].last_tokens[topic_name]
