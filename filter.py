import sys


if __name__ == '__main__':
  for line in sys.stdin:
    if len(line.split()) <= 1: continue
    sys.stdout.write(line)
