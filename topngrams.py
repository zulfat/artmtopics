import codecs
from glob import glob


def update_bigrams(bigrams, inputfpath):
  with codecs.open(inputfpath) as input_stream:
    for line in input_stream:
      for ngram_count in line.split()[1:]:
        ngram, count = ngram_count.split(':')
        if '_' not in ngram: continue
        if ngram not in bigrams: bigrams[ngram] = 0
        bigrams[ngram] += 1


def filter_data(inputfpath, bigrams):
  with codecs.open(inputfpath) as input_stream:
    collection = input_stream.readlines()
  with codecs.open(inputfpath, 'w') as output_stream:
    for document in collection:
      doc_parts = document.split()
      new_doc = [doc_parts[0]]
      for ngram_count in doc_parts[1:]:
        ngram, count = ngram_count.strip().split(':')
        if '_' not in ngram or ngram in bigrams: new_doc.append(ngram_count)
      new_doc = ' '.join(new_doc)
      output_stream.write('{}\n'.format(new_doc))


if __name__ == '__main__':
  inputfpattern = '/data/solidopinion/vw/*.csv'
  bigrams = {}
  topn = 10000
  for inputfpath in glob(inputfpattern):
    print inputfpath
    update_bigrams(bigrams, inputfpath)
  print '-'*20
  bigrams = sorted(bigrams, key=lambda ngram: bigrams[ngram], reverse=True)[:topn]
  for inputfpath in glob(inputfpattern):
    print inputfpath
    filter_data(inputfpath, bigrams)
