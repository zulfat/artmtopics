class ProcessMaintainer(object):

    def __init__(self, nprocesses=4):
        self.nprocesses = nprocesses
        self.jobs = []

    def add_task(self, process):
        if len(self.jobs) >= self.nprocesses:
            jprocess = self.jobs.pop(0)
            jprocess.join()
        self.jobs.append(process)
        process.start()

    def join_remaining_tasks(self):
        for job in self.jobs:
            job.join()

    def kill_all_tasks(self):
        pass
