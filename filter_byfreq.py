import codecs
from glob import glob
from joblib import Parallel, delayed


def update_stats(stats, inputfpath):
  ngram_stats = stats['ngram_stats']
  with codecs.open(inputfpath) as input_stream:
    for document in input_stream:
      for ngram_count in document.split()[1:]:
          ngram, count = ngram_count.split(':')
          if ngram not in ngram_stats: ngram_stats[ngram] = {'term_freq': 0,
                                                              'doc_freq': 0}
          ngram_stats[ngram]['term_freq'] += int(count)
          ngram_stats[ngram]['doc_freq'] += 1
      stats['total_docs'] += 1


def filter_data(inputfpath, ngrams):
  with codecs.open(inputfpath) as input_stream:
    collection = input_stream.readlines()
  with codecs.open(inputfpath, 'w') as output_stream:
    for document in collection:
      doc_parts = document.split()
      new_doc = [doc_parts[0]]
      for ngram_count in doc_parts[1:]:
        ngram, count = ngram_count.strip().split(':')
        if ngram in ngrams: new_doc.append(ngram_count)
      new_doc = ' '.join(new_doc)
      output_stream.write('{}\n'.format(new_doc))


if __name__ == '__main__':
  stats = {
      'ngram_stats':{},
      'total_docs': 0
  }
  inputfpattern = ''
  min_tf = 10
  max_df = 0.3
  for inputfpath in glob(inputfpattern):
    update_stats(ngram_stats, inputfpath)

  ngrams = [ngram for ngram, ngram_stats in stats.items() if ngram_stats['term_freq'] >= min_tf and \
                                                             1.0*ngram_stats['doc_freq']/stats['total_docs'] <= max_df]

   Parallel(n_jobs=4)(delayed(filter_data)(inputfpath, ngrams) for inputfpath in glob(inputfpattern)